#ifndef _BLINKER_H_
#define _BLINKER_H_

#include "stdint.h"
#include "bsp.h"
typedef struct led_data
{
    void* gpio;
    uint32_t pin_mask;
    uint16_t led_speed;
} led_data_t ;


uint8_t led_register_element (led_data_t *led_handle);


#endif /* _BLINKER_H_ */