#include "blinker.h"

#define SET_PIN(GPIOx, PIN)             (GPIOx->BSHR = 1<<PIN)        
#define RESET_PIN(GPIOx, PIN)           (GPIOx->BCR = 1<<PIN) 

led_data_t *handle;

uint8_t led_register_element(led_data_t *led_handle)
{
    GPIO_InitTypeDef GPIO_InitStructure = {0};

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

    Delay_Init();

    uint32_t delay = led_handle->led_speed/2 ; 
    uint32_t pin = 1<<led_handle->pin_mask;
    GPIO_TypeDef* GPIOx = (GPIO_TypeDef*)(led_handle->gpio);
    int state = 0; 
    while(1)
    {
        GPIO_WriteBit(GPIOx, pin, 1);
        Delay_Ms(delay);
        GPIO_WriteBit(GPIOx, pin, 0);
        Delay_Ms(delay);
    }
}