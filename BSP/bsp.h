#ifndef _MBSP_H_
#define _MBSP_H_

#if (ESP32_WROOM_32)
    #include "blinker.h"
    
#elif (STM32F1xx)

#include <stm32f1xx_ll_bus.h>
#include <stm32f1xx_ll_rcc.h>
#include <stm32f1xx_ll_system.h>
#include <stm32f1xx_ll_utils.h>
#include <stm32f1xx_ll_gpio.h>

#include <led_lib.h>

// #define LED_PIN                           LL_GPIO_PIN_13 //${LED_PIN}
// #define LED_GPIO_PORT                     GPIOC //${LED_GPIO}
// #define LED_SPEED                         100


#elif (CH32V00x)
    #include "system_ch32v00x.h"
    #include <debug.h>
    #include "ch32v00x_gpio.h"
    #include "blinker.h"
#else
    #error Unsupported MCU Target 
#endif


#endif