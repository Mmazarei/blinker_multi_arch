#include <led_lib.h>


void togle_pin(led_data_t *LED)
{
    LL_GPIO_TogglePin(LED->port, LED->pin_mask);
}

void delay(uint16_t delay)
{
        HAL_Delay(delay);
}

void led_register_element(led_data_t *LED)
{
    if (LED->port==GPIOA)
        LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
    else if (LED->port== GPIOB)
        LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
    else if (LED->port== GPIOC)
        LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
    else LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
    
    // LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
    LL_GPIO_SetPinMode(LED->port, LED->pin_mask, LL_GPIO_MODE_OUTPUT);

    while(1)
    {
        togle_pin(LED);
        delay(LED->led_speed);
    }
}
