#ifndef LED_LIB_H
#define LED_LIB_H

#include <bsp.h>

typedef struct led_data
{
    GPIO_TypeDef* port;
    uint32_t pin_mask;
    uint16_t led_speed;
} led_data_t ;


void led_register_element(led_data_t *LED);
void togle_pin(led_data_t *LED);
void delay(uint16_t delay);

#endif
