#ifndef _BLINKER_H_
#define _BLINKER_H_

#include <stdio.h>
#include <stdint.h>
    


/* Task configurations */
#define LED_TOGGLE_TASK_STACK_SIZE          (2*1024)
#define LED_TOGGLE_TASK_PRIO                (10)    
#define LED_TOGGLE_TASK_CORE                (1)


typedef struct led_data
{
    uint32_t gpio;
    uint32_t pin_mask;
    uint16_t led_speed;
} led_data_t ;


uint8_t led_register_element (led_data_t *led_handle);
// void led_unregister_element (void);


#endif