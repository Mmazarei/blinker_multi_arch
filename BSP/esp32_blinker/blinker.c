#include "stdio.h"
#include "stdint.h"
#include "blinker.h"
#include "driver/gpio.h"
#include "unistd.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
#include "esp_system.h"
#include "nvs.h"
#include "nvs_flash.h"


led_data_t *handle = NULL;

uint8_t led_register_element(led_data_t *led_handle)
{
    handle = led_handle;
    if (handle == NULL)
        return 0;
    printf("led data -> pin :%d , speed = %d\n", handle->pin_mask, handle->led_speed);
    gpio_set_direction((gpio_num_t)handle->pin_mask, GPIO_MODE_OUTPUT);
    gpio_set_level((gpio_num_t)handle->pin_mask, 1);
    uint8_t cnt = 0;
    while (1)
    {
        gpio_set_level((gpio_num_t)handle->pin_mask, (cnt++) & 0x1);
        usleep ((useconds_t)(handle->led_speed * 500));
    }
    
}