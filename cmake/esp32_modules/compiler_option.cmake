message(STATUS "************ ESP32 cmake module **************")

#
# Changing src directory to new dir
#
set (EXTRA_COMPONENT_DIRS 
    "${CMAKE_SOURCE_DIR}/src/esp32_cmake/"
)

#
#  Adding directory for custom library and components
#
set (CUSTOM_LIB_DIRS
    "${CMAKE_SOURCE_DIR}/BSP/"
    "${CMAKE_SOURCE_DIR}/BSP/esp32_blinker/"
)
list(APPEND EXTRA_COMPONENT_DIRS ${CUSTOM_LIB_DIRS})


#
# adding cmake configurations 
#
include ($ENV{IDF_PATH}/tools/cmake/project.cmake)
