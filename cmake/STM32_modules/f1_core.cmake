set(SYS_SRC_FILES 
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/initialize-hardware.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/cmsis/system_stm32f1xx.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/cmsis/vectors_stm32f103xb.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/cortexm/exception-handlers.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/cortexm/initialize-hardware.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/cortexm/reset-hardware.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/diag/trace-impl.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/diag/trace.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/assert.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/cxx.cpp
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/exit.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/sbrk.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/startup.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/src/newlib/syscalls.c    
        # HAL Library Source
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_eth.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_dma.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_dac_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_dac.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_crc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_cortex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_cec.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_can.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_adc_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_adc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_iwdg.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_irda.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_i2s.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_i2c.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_hcd.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_gpio_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_gpio.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_flash_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_flash.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_exti.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_smartcard.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_sd.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_rtc_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_rtc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_rcc_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_rcc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_pwr.c
        # f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_pcd_ex.c
        # f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_pcd.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_pccard.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_nor.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_nand.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_mmc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_wwdg.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_usart.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_uart.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_tim_ex.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_tim.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_sram.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_hal_spi.c
        # LL Library Source
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_rcc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_pwr.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_i2c.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_gpio.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_fsmc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_exti.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_dma.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_dac.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_crc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_adc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_utils.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_usb.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_usart.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_tim.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_spi.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_sdmmc.c
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Src/stm32f1xx_ll_rtc.c
)

set(SYS_INC_FILES
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core    
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/include
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/include/cmsis
        ${CMAKE_SOURCE_DIR}/system/STM32/f1_core/stm32f1xx_hal_driver/Inc
)

##########################################################################################################################

set(PORT_LIST
    GPIOA
    GPIOB
    GPIOC
    GPIOD)

set(LED_GPIO GPIOA CACHE STRING "This port is Default on blue pill")
set_property(CACHE LED_GPIO PROPERTY STRINGS ${PORT_LIST})
add_definitions( -DLED_GPIO=${LED_GPIO} )

set(PIN_LIST
    LL_GPIO_PIN_0
    LL_GPIO_PIN_1
    LL_GPIO_PIN_2
    LL_GPIO_PIN_3
    LL_GPIO_PIN_4
    LL_GPIO_PIN_5
    LL_GPIO_PIN_6
    LL_GPIO_PIN_7
    LL_GPIO_PIN_8
    LL_GPIO_PIN_9
    LL_GPIO_PIN_10
    LL_GPIO_PIN_11
    LL_GPIO_PIN_12
    LL_GPIO_PIN_13
    LL_GPIO_PIN_14
    LL_GPIO_PIN_15
    LL_GPIO_PIN_16
)

set(LED_PIN GPIO_PIN_0 CACHE STRING "This pin is Default on blue pill")
set_property(CACHE LED_PIN PROPERTY STRINGS ${PIN_LIST})
add_definitions( -DLED_PIN=${LED_PIN} )

set(LED_SPEED= 100)
set(LED_SPEED 100 CACHE STRING 0)
add_definitions( -DLED_SPEED=${LED_SPEED} )
