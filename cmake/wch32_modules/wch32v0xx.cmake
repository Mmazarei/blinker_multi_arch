message("-------------------compiler options-------------------")

set(FLASH_SIZE "16K" CACHE STRING "microcontroller FLASH size")
set(RAM_SIZE  "2K" CACHE STRING "microcontroller RAM size")

#Cpu specific paramiters
set (CPU_PARAMETERS
    -march=rv32ecxw
    -mabi=ilp32e
    -msmall-data-limit=0
    -msave-restore
)

# compiler option
set(TARGET_MCU_COMPILER
    ${CPU_PARAMETERS}
    -std=gnu99
    -Os
    -g3
    -fmessage-length=0
    -fsigned-char
    -ffunction-sections
    -fdata-sections
    -Wno-unused-parameter
    -ffreestanding
    -fno-move-loop-invariants
    -Wall
    -Wextra
    -fno-common

    -Wunused
    -Wuninitialized

)

# linker option
set(TARGET_MCU_LINKER
    -fmessage-length=0
    -fsigned-char
    -ffunction-sections
    -Wall
    -nostartfiles
    -Xlinker --gc-sections

    # -fdata-sections
    # -fno-common
    # -Wunused
    # -Wuninitialized
    #-nostartfiles
    #-Xlinker --gc-sections

    --specs=nano.specs
    --specs=nosys.specs

    -Wl,--print-memory-usage
    -lprintf
    # -Wl,--start-group
    # -lc
    # -lm
    # -lstdc++
    # -lsupc++
    # -Wl,--end-group
    # -Wall
)

# target define
set(TARGET_MCU_DEF
    -DDEBUG
    -D${MCU}
)
