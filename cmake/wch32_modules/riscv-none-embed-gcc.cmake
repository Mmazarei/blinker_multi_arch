
set(CMAKE_SYSTEM_NAME          Generic)
set(CMAKE_SYSTEM_PROCESSOR     riscv32)

set(TOOLCHAIN_PREFIX                "/home/tarahi/MounRiver_Studio_Community_Linux_x64_V140/MRS_Community/toolchain/RISC-V Embedded GCC/bin/riscv-none-embed-")

set(FLAGS                           -fdata-sections
                                    -ffunction-sections
                                    -Wl,--gc-sections
)

set(CPP_FLAGS                       -fno-rtti
                                    -fno-exceptions
                                    -fno-threadsafe-statics
)
set(ASM_FLAGS
                                    -x assembler-with-cpp -I"${CMAKE_SOURCE_DIR}/system/CH32/startup_ch32v00x.s"
)

set(CMAKE_TRY_COMPILE_TARGET_TYPE   STATIC_LIBRARY)

set(CMAKE_C_COMPILER                ${TOOLCHAIN_PREFIX}gcc)
set(CMAKE_CXX_COMPILER              ${TOOLCHAIN_PREFIX}g++)
set(CMAKE_ASM_COMPILER              ${CMAKE_C_COMPILER})

# set(CMAKE_C_COMPILER                "/home/tarahi/MounRiver_Studio_Community_Linux_x64_V140/MRS_Community/toolchain/RISC-V Embedded GCC/bin/riscv-none-embed-gcc")
# set(CMAKE_CXX_COMPILER              "/home/tarahi/MounRiver_Studio_Community_Linux_x64_V140/MRS_Community/toolchain/RISC-V Embedded GCC/bin/riscv-none-embed-g++")
# set(CMAKE_ASM_COMPILER              ${CMAKE_C_COMPILER})


# set(CMAKE_OBJCOPY                   "/home/tarahi/MounRiver_Studio_Community_Linux_x64_V140/MRS_Community/toolchain/RISC-V Embedded GCC/bin/riscv-none-embed-objcopy"      CACHE INTERNAL "objcopy tool")
# set(CMAKE_SIZE_UTIL                 "/home/tarahi/MounRiver_Studio_Community_Linux_x64_V140/MRS_Community/toolchain/RISC-V Embedded GCC/bin/riscv-none-embed-size"         CACHE INTERNAL "size tool")
set(CMAKE_OBJCOPY                   ${TOOLCHAIN_PREFIX}objcopy)
set(CMAKE_SIZE_UTIL                 ${TOOLCHAIN_PREFIX}size)


# set(CMAKE_FIND_ROOT_PATH                ${BINUTILS_PATH})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM   NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY   ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE   ONLY)