set (SYS_SRC_FILES
        #Startup file
        ${CMAKE_SOURCE_DIR}/system/CH32/startup_ch32v00x.S
        #core
        ${CMAKE_SOURCE_DIR}/system/CH32/v0_core/src/ch32v00x_it.c
        ${CMAKE_SOURCE_DIR}/system/CH32/v0_core/src/system_ch32v00x.c
        ${CMAKE_SOURCE_DIR}/system/CH32/v0_core/src/core/core_riscv.c
        #Driver
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_adc.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_dbgmcu.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_dma.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_exti.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_flash.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_gpio.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_i2c.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_iwdg.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_misc.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_opa.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_pwr.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_rcc.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_spi.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_tim.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_usart.c
        ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/src/ch32v00x_wwdg.c
)

set (SYS_INC_FILES
    ${CMAKE_SOURCE_DIR}/system/CH32/v0_core/include
    ${CMAKE_SOURCE_DIR}/system/CH32/v0_core/include/core
    ${CMAKE_SOURCE_DIR}/system/CH32/wch32v003_driver/include
)

list (APPEND    SRC_FILES   ${SYS_SRC_FILES})
list (APPEND    INC_FILES   ${SYS_INC_FILES})
